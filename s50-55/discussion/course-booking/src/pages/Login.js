import {Button, Col, Container, Form, Row} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Navigate, Link, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext.js';

// import sweet alert
import Swal2 from 'sweetalert2';


export default function Login(){

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);
	// The getItem() method gets the value of the specified key from our local storage
	// const [user, setUser] = useState(localStorage.getItem('email'))
	const navigate = useNavigate();

	// we are going to consume or use the UserContext
	const {setUser} = useContext(UserContext);

	useEffect(() => {

		if(email !== '' && password !== ''){

			setIsDisabled(false);

		} else {

			setIsDisabled(true);

		}
	}, [email, password])

	function login(e){

		e.preventDefault();

		// process a fetch request to the corresponding backend API
		// Syntax:
			// fetch(url, {option})
		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: "POST",
			headers: {
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(result => result.json())
		.then(data => {
			if(data === false){
				
				Swal2.fire({
					title: "Authentication failed!",
					icon: "error",
					text: "Check your login details and try again"
				})
			} else {

				localStorage.setItem("token", data.auth)
				retrieveUserDetails(data.auth);

				Swal2.fire({
					title : "Login Successful",
					icon : "success",
					text: "Welcome to Zuitt!"
				})
				navigate("/");

			}
		})
}

		// Set the email of the authenticated user in the local storage
		// Syntax:
			// localStorage.setItem('propertyName', value)

		// localStorage.setItem('email', email)

		// setUser(localStorage.getItem('email'))

		// navigate("/")

		// setLogin()

		// setEmail('');
		// setPassword('');
	


	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
			method: "GET",
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(data => {
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}


return(
		<Container className = "mt-5 mb-5">
			<Row>
				<Col className = "col-6 mx-auto">
					<h1 className = "text-center">Login</h1>
					<Form onSubmit = {e => login(e)}>
					     <Form.Group className="mb-3" controlId="formBasicEmail">
					       <Form.Label>Email address</Form.Label>
					       <Form.Control type="email" value = {email} onChange = {e => setEmail(e.target.value)} placeholder="Enter email" />
					     </Form.Group>

					     <Form.Group className="mb-3" controlId="formBasicPassword">
					       <Form.Label>Password</Form.Label>
					       <Form.Control type="password" value = {password} onChange = {e => setPassword(e.target.value)} placeholder="Password" />
					     </Form.Group>
					     <p>No account yet? <Link to = "/register">Sign up here</Link></p>
					     <Button variant="primary" type="submit" disabled = {isDisabled}>
					       Submit
					     </Button>
					   </Form>
				</Col>
			</Row>
		</Container>
	)
}