import {Button, Col, Container, Form, Row} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Link, useNavigate} from 'react-router-dom';

import UserContext from '../UserContext.js';

import Swal2 from 'sweetalert2';


export default function Register(){

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNumber, setMobileNumber] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isDisabled, setIsDisabled] = useState(true)

	// we consume the setUser function from the UserContext
	const {setUser} = useContext(UserContext); 

	// we contain useNavigate to navigate variagble
	const navigate = useNavigate();
	// We have to use the useEffect in enabling the submit button
	useEffect(() => {
		if(firstName !== '' && lastName !== '' && mobileNumber.length > 10 && email !== '' && password1 !== '' && password2 !== '' && password1 === password2 && password1.length > 5){

			setIsDisabled(false);

		} else {

			setIsDisabled(true);

		}
	}, [email, firstName, lastName, mobileNumber, password1, password2])

	// function that wil be triggered once we submit
	function register(event){

		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: "POST",
			headers: {
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNumber,
				password: password1
			})	
		})
		.then(result => result.json())
		.then(data => {

			if(!data){

				Swal2.fire({
					title: "Email has been taken",
					icon: "error",
					text: "Please use another email!"
				})

			} else {

				Swal2.fire({
					title: "Registration successfull",
					icon: "success",
					text: "Welcome to Zuitt!"
				})

				navigate('/login')
			}
		})
	}
return(
	<Container className = "mt-5 mb-5">
		<Row>
			<Col className = "col-6 mx-auto">
				<h1 className = "text-center">Register</h1>
				<Form onSubmit = {event => register(event)}>

					<Form.Group className="mb-3" controlId="validationCustom1">
				        <Form.Label>First Name</Form.Label>
				        <Form.Control 
				        	type="text"
				        	value = {firstName} 
				        	onChange = {event => {
				        	
				        		setFirstName(event.target.value)
				        	}}
				        	placeholder="Enter first name" />

				    </Form.Group>

					<Form.Group className="mb-3" controlId="validationCustom2">
				        <Form.Label>Last Name</Form.Label>
				        <Form.Control 
				        	type="text"
				        	value = {lastName} 
				        	onChange = {event => {
				        	
				        		setLastName(event.target.value)

				        	}}
				        	placeholder="Enter last name" />

				    </Form.Group>

				    <Form.Group className="mb-3" controlId="formBasicEmail">
				        <Form.Label>Email Address</Form.Label>
				        <Form.Control 
				        	type="email"
				        	value = {email} 
				        	onChange = {event => {
				        	
				        		setEmail(event.target.value)
				        		console.log(email)
				        	}}
				        	placeholder="Enter email" />

				    </Form.Group>

					<Form.Group className="mb-3" controlId="formBasicEmail">
				        <Form.Label>Mobile No.</Form.Label>
				        <Form.Control 
				        	type="number"
				        	value = {mobileNumber} 
				        	onChange = {event => {
				        	
				        		setMobileNumber(event.target.value)
				        	}}
				        	placeholder="Enter mobile number" />

				    </Form.Group>

				    <Form.Group className="mb-3" controlId="formBasicPassword1">
				        <Form.Label>Password</Form.Label>
				        <Form.Control 
				        	type="password" 
				        	value = {password1} 
				        	onChange = {event => {
				        	
				        		setPassword1(event.target.value)

				        	}}
				        	placeholder="Password" />
				    </Form.Group>

				    <Form.Group className="mb-3" controlId="formBasicPassword2">
				        <Form.Label>Confirm Password</Form.Label>
				        <Form.Control 
				        	type="password" 
				        	value = {password2}
				        	onChange = {event => {
				        	
				        		setPassword2(event.target.value)

				        	}} 
				        	placeholder="Confirm Password" />
				    </Form.Group>

				    <p>Have an account already ? <Link to = "/login">Log in here</Link></p>
				    <Button variant="primary" type="submit" disabled = {isDisabled}>Submit</Button>
				</Form>
			</Col>
		</Row>
	</Container>
	)
}