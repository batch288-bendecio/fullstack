import {Link} from 'react-router-dom';
import {Container} from 'react-bootstrap'

export default function PageNotFound(){


	return(
		<Container>
			<p className = "fs-5 mt-5">Zuitt Booking</p>
			<h1>Page Not Found</h1>
			<p>Go back to the <Link to = "/">homepage.</Link></p>
		</Container>
		)
}