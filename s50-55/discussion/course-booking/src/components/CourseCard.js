import {Button, Card, Col, Container, Row} from 'react-bootstrap';
// import the useState hook from react
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext.js';
import {Link} from 'react-router-dom'



export default function CourseCard(props){

	// Consume the content of the UserContext.

	const {user} = useContext(UserContext);

	// Object destructuring

	const {_id, name, description, price} = props.courseProp;


	// use the state hook for this component to be able to store its state
	// states are use to keep track of information related to individual components

		/*
			Syntax:
				const [getter, setter] = useState(initialGetterValue);
		*/

	const [count, setCount] = useState(0);
	const [seat, setSeat] = useState(30)
	const [isDisabled, setIsDisabled] = useState(false);
	// setCount(2);
	// console.log(count);

	function enroll(){
	
		if(seat !== 0){

			setCount(count + 1)
			setSeat(seat - 1)

		} else {

			return alert("No seats available!")
		}
	}

	// the function or the side effect in our useEffect hook wil invoke or run on the initial loading of our application and when there is/are change/changes on our dependencies.
	useEffect( () => {
		if(seat === 0){
			setIsDisabled(true);
		}
	}, [seat]);

	return( 
		<Container className = "mt-3"> 
			<Row>
				<Col> 
					<Card className = "mt-3 mb-3">
						<Card.Body>
							<Card.Title>{name}
							</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>PHP {price}</Card.Text>
					        <Card.Subtitle>Enrollees:</Card.Subtitle>
					        <Card.Text>{count}</Card.Text>
					      {
					      	user !== null ?
					      	<Button variant="primary" as = {Link} to = {`/courses/${_id}`}>More details</Button>
					      	:
					      	<Button as = {Link} to = "/login">Login to enroll!</Button>
					      }
					    </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
		)
}