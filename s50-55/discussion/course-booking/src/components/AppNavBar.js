import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import UserContext from '../UserContext.js'
// We use Link and NavLink to add hyperlink to our application, we use NavLink for navigation bar.

import {Link, NavLink} from 'react-router-dom';
import {useContext} from 'react';

// the "as" allows components to be treated as if they are a different component gaining the access to its properties and functionalities

export default function AppNavBar(){

// const [user, setUser] = useState(localStorage.getItem('email'));

	// We will consume the user state from the global context

	const {user} = useContext(UserContext)

	return(
		<>
		   	<Navbar bg="dark" variant="dark">
		      	<Container>
		         	<Navbar.Brand as = {Link} to = "/">Zuitt</Navbar.Brand>
		         	<Nav className="ms-auto">
		           	<Nav.Link as = {NavLink} to = "/">Home</Nav.Link>
		           	<Nav.Link as = {NavLink} to = "/courses">Courses</Nav.Link>
		          {
		          	user.id === null ? 
		          	<>
		          		<Nav.Link as = {NavLink} to = "/register">Register</Nav.Link>
		          		<Nav.Link as = {NavLink} to = "/login">Login</Nav.Link>
		          	</> :
		          	<Nav.Link as = {NavLink} to = "/logout">Logout</Nav.Link>
		          }
		         	</Nav>
		   		</Container>
		 	</Navbar>
		</>
		)
}