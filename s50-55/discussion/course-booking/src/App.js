import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import AppNavBar from './components/AppNavBar.js';
import Courses from './pages/Courses.js';
import Home from './pages/Home.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import PageNotFound from './pages/PageNotFound.js'
import CourseView from './pages/CourseView.js'


// The BrowserRouter component will enable us to simulate page navigation by synchronizing the shown content and the shown URL in the web browser.
// The Routes component holds all our Route components. It select which 'Route' component to show based on the url endpoint.
import {BrowserRouter, Route, Routes} from 'react-router-dom';
import {useState, useEffect} from 'react';

// import UserProvider
import {UserProvider} from './UserContext.js';


function App() {

  let userDetails = {};

  useEffect(()=>{

    if(localStorage.getItem('token') === null){

      userDetails = {
        id: null,
        isAdmin: null
      }

    } else {

        fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
          method: 'GET',
          headers: {
              Authorization: `Bearer ${localStorage.getItem('token')}`
          }
      })
      .then(result => result.json())
      .then(data => {
          userDetails = {
              id : data._id,
              isAdmin: data.isAdmin
          };

      })
    }

  }, [])

    const [user, setUser] = useState(userDetails);

  const unsetUser = () => {

    localStorage.clear()

  }


  return (

  <UserProvider value = {{user, setUser, unsetUser}}>
    <BrowserRouter>
        <AppNavBar/>
        <Routes>
          <Route path = "/" element = {<Home/>} />

          <Route path = "/courses" element = {<Courses/>} />

          <Route path = "/register" element = {user.id === null ? <Register/> : <PageNotFound/>} />

          <Route path = "/login" element = {user.id === null ? <Login/> : <PageNotFound/>}/>

          <Route path = "/logout" element = {<Logout/>} />
          <Route path = "/courses/:courseId" element = {<CourseView/>} />
          <Route path = "*" element = {<PageNotFound/>} />
        </Routes>
    </BrowserRouter>
  </UserProvider>

   );
}

export default App;