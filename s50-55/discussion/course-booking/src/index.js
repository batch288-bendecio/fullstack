import App from './App';
import React from 'react';
import ReactDOM from 'react-dom/client';


// createRoot() - assigns the element to be managed by React with its virtual DOM

const root = ReactDOM.createRoot(document.getElementById('root'));

// render() - displaying the component/react element into the root
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// const name = "John";
// const element = <h1>Hello, {name}! </h1>

// root.render(element);